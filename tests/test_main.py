"""Tests for umb_messenger.__main__."""
import unittest
from unittest import mock

from tests import fakes
from umb_messenger import settings
import umb_messenger.__main__ as main


class TestProcessMessage(unittest.TestCase):
    """Tests for __main__.process_message()."""

    @mock.patch('umb_messenger.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    @mock.patch('umb_messenger.umb.handle_message')
    def test_post_test(self, mock_umb_handle, mock_checkout):
        """Verify post_test messages are sent correctly."""
        payload = {'status': 'ready_to_report', 'object_type': 'checkout', 'id': 'redhat:123'}
        mock_checkout.return_value = fakes.get_fake_checkout(
            actual_attrs={'valid': True},
            misc={}
        )
        main.process_message(body=payload)
        mock_umb_handle.assert_called()

    def test_retrigger(self):
        """Verify messages are not sent for retriggered checkouts."""
        payload = {'status': 'ready_to_report', 'object_type': 'checkout', 'id': 'redhat:123',
                   'object': {'data': 'something', 'misc': {'retrigger': True}}}

        with self.assertLogs(level='DEBUG', logger=settings.LOGGER) as logs:
            main.process_message(body=payload)
            self.assertIn('Retriggered checkout', logs.output[-1])

    @mock.patch('umb_messenger.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    @mock.patch('umb_messenger.umb.handle_message')
    def test_pre_test(self, mock_umb_handle, mock_checkout):
        """Verify pre_test messages are sent correctly."""
        payload = {'status': 'build_setups_finished', 'object_type': 'checkout', 'id': 'redhat:123'}
        mock_checkout.return_value = fakes.get_fake_checkout(
            actual_attrs={'valid': True},
            misc={}
        )
        main.process_message(body=payload)
        mock_umb_handle.assert_called()

    @mock.patch('umb_messenger.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    @mock.patch('umb_messenger.umb.handle_message')
    def test_invalid_checkout(self, mock_umb_handle, mock_checkout):
        """Verify no messages are sent if the checkout is not valid."""
        payload = {'status': 'build_setups_finished', 'object_type': 'checkout', 'id': 'redhat:123'}
        mock_checkout.return_value = fakes.get_fake_checkout(
            misc={}, actual_attrs={'valid': False}
        )
        main.process_message(body=payload)
        mock_umb_handle.assert_not_called()
